## The Artisan
Official repository for the game "The Artisan", by the Windmakers. This is a
project guided by [GameDev Society UFRGS](http://gdsufrgs.com).

More info can be found in our [official development blog](http://windmakers.weebly.com).
